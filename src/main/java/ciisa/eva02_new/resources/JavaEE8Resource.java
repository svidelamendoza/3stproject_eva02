package ciisa.eva02_new.resources;

import java.util.concurrent.ExecutorService;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.container.AsyncResponse;
import javax.ws.rs.container.Suspended;
import javax.ws.rs.core.Response;

/**
 *
 * @author 
 */
@Path("javaee8")
public class JavaEE8Resource {

    private final ExecutorService executorService = java.util.concurrent.Executors.newCachedThreadPool();
    
    @GET
    public void ping(@Suspended
    final AsyncResponse asyncResponse){
        executorService.submit(() -> {
            asyncResponse.resume(doPing());
        });
    }

    private Response doPing() {
        return Response
                .ok("ping")
                .build();
    }
}
