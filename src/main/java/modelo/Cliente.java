/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Sthephania
 */
@Entity
@Table(name = "clientes")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Cliente.findAll", query = "SELECT c FROM Cliente c"),
    @NamedQuery(name = "Cliente.findByCliID", query = "SELECT c FROM Cliente c WHERE c.cliID = :cliID"),
    @NamedQuery(name = "Cliente.findByCliRut", query = "SELECT c FROM Cliente c WHERE c.cliRut = :cliRut"),
    @NamedQuery(name = "Cliente.findByCliDV", query = "SELECT c FROM Cliente c WHERE c.cliDV = :cliDV"),
    @NamedQuery(name = "Cliente.findByCliNombres", query = "SELECT c FROM Cliente c WHERE c.cliNombres = :cliNombres"),
    @NamedQuery(name = "Cliente.findByCliApPaterno", query = "SELECT c FROM Cliente c WHERE c.cliApPaterno = :cliApPaterno"),
    @NamedQuery(name = "Cliente.findByCliApMaterno", query = "SELECT c FROM Cliente c WHERE c.cliApMaterno = :cliApMaterno")})
public class Cliente implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @Column(name = "Cli_ID")
    private String cliID;
    @Column(name = "Cli_Rut")
    private String cliRut;
    @Column(name = "Cli_DV")
    private String cliDV;
    @Column(name = "Cli_Nombres")
    private String cliNombres;
    @Column(name = "Cli_Ap_Paterno")
    private String cliApPaterno;
    @Column(name = "Cli_Ap_Materno")
    private String cliApMaterno;

    public Cliente() {
    }

    public Cliente(String cliID) {
        this.cliID = cliID;
    }

    public String getCliID() {
        return cliID;
    }

    public void setCliID(String cliID) {
        this.cliID = cliID;
    }

    public String getCliRut() {
        return cliRut;
    }

    public void setCliRut(String cliRut) {
        this.cliRut = cliRut;
    }

    public String getCliDV() {
        return cliDV;
    }

    public void setCliDV(String cliDV) {
        this.cliDV = cliDV;
    }

    public String getCliNombres() {
        return cliNombres;
    }

    public void setCliNombres(String cliNombres) {
        this.cliNombres = cliNombres;
    }

    public String getCliApPaterno() {
        return cliApPaterno;
    }

    public void setCliApPaterno(String cliApPaterno) {
        this.cliApPaterno = cliApPaterno;
    }

    public String getCliApMaterno() {
        return cliApMaterno;
    }

    public void setCliApMaterno(String cliApMaterno) {
        this.cliApMaterno = cliApMaterno;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (cliID != null ? cliID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Cliente)) {
            return false;
        }
        Cliente other = (Cliente) object;
        return !((this.cliID == null && other.cliID != null) || (this.cliID != null && !this.cliID.equals(other.cliID)));
    }

    @Override
    public String toString() {
        return "modelo.Cliente[ cliID=" + cliID + " ]";
    }



}
