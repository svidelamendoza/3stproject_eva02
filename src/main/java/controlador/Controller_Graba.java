/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import ciisa.eva02_new.ClienteDAO;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import modelo.Cliente;

/**
 *
 * @author Sthephania
 */
@WebServlet(name = "Controller_Graba", urlPatterns = {"/controller_Graba"})
public class Controller_Graba extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet Controller_Graba</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet Controller_Graba at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String accion = request.getParameter("accion");
        if (accion.equalsIgnoreCase("grabarEditar")) {
            try {

                ClienteDAO dao = new ClienteDAO();

                String cli_id = request.getParameter("cli_id");
                String cli_rut = request.getParameter("cli_rut");
                String cli_dv = request.getParameter("cli_dv");
                String cli_nombres = request.getParameter("cli_nombres");
                String cli_ap_paterno = request.getParameter("cli_ap_paterno");
                String cli_ap_materno = request.getParameter("cli_ap_materno");

                Cliente cli = new Cliente();
                
               cli.setCliID(cli_id);
               cli.setCliRut(cli_rut);
               cli.setCliDV(cli_dv);
               cli.setCliNombres(cli_nombres);
               cli.setCliApPaterno(cli_ap_paterno);
               cli.setCliApMaterno(cli_ap_materno);



                dao.edit(cli);

                List<Cliente> clientes = dao.findClienteEntities();
                System.out.println("Cantidad CLIENTES en BD  " + clientes.size());

                request.setAttribute("clientes", clientes);
                request.getRequestDispatcher("controller_list").forward(request, response); //A OBSERVAR...
            } catch (Exception ex) {
                Logger.getLogger(Controller_editar.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        //processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
