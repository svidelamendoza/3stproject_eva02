/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;


import ciisa.eva02_new.ClienteDAO;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import modelo.Cliente;

/**
 *
 * @author Sthephania
 */
@WebServlet(name = "Controller", urlPatterns = {"/controller"})
public class Controller extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    //Este servlet es el controlador para registrar los elementos a la BD
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        //Asignacion de los valores enviados por metodo post:
       
       String Id = request.getParameter ("cli_id");
       String Rut = request.getParameter ("cli_rut");
       String Dv = request.getParameter ("cli_dv");
       String Nombres = request.getParameter ("cli_nombres");
       String ApPaterno = request.getParameter ("cli_ap_paterno");
       String ApMaterno = request.getParameter ("cli_ap_materno");
       
             
       //Creación Objeto Entity:
       
        Cliente clte = new Cliente ();
        clte.setCliID(Id);
        clte.setCliRut(Rut);
        clte.setCliDV(Dv);
        clte.setCliNombres(Nombres);
        clte.setCliApPaterno(ApPaterno);
        clte.setCliApMaterno(ApMaterno);        
       
      //Creación Objeto DAO:
       
       ClienteDAO dao = new ClienteDAO ();
       try {
       dao.create(clte);
       Logger.getLogger("log").log(Level.INFO, "valor ID cliente: {0}", clte.getCliID());
       }catch (Exception ex){
           Logger.getLogger ("log").log (Level.SEVERE, "{0}Error al ingresar dato", ex.getMessage());
           
         }
       
       request.getRequestDispatcher("salida.jsp").forward (request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
