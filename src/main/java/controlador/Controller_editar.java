/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import ciisa.eva02_new.ClienteDAO;
import ciisa.eva02_new.resources.NonexistentEntityException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import modelo.Cliente;

/**
 *
 * @author Sthephania
 */
@WebServlet(name = "Controller_Editar", urlPatterns = {"/controller_editar"})
public class Controller_editar extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {


    }

// <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String seleccion = request.getParameter("seleccion");
        String accion = request.getParameter("accion");
        System.out.println("controllerList  seleccion: " + seleccion);
        System.out.println("controllerList  accion: " + accion);

        if (accion.equalsIgnoreCase("eliminar")) {
            ClienteDAO dao = new ClienteDAO();
            List<Cliente> clientes = new ArrayList<Cliente>();
            try {
                dao.destroy(seleccion);
                clientes = dao.findClienteEntities();
            } catch (NonexistentEntityException ex) {
                Logger.getLogger(Controller_list.class.getName()).log(Level.SEVERE, null, ex);
            }
            request.setAttribute("clientes", clientes);
            request.getRequestDispatcher("controller_list").forward(request, response);
        }

        if (accion.equalsIgnoreCase("crear")) {
            request.getRequestDispatcher("registrar.jsp").forward(request, response);
        }

        if (accion.equalsIgnoreCase("editar")) {

            Cliente cli = new Cliente();
            ClienteDAO dao = new ClienteDAO();

            cli = dao.findCliente(seleccion);
            request.setAttribute("cliente", cli);
            request.getRequestDispatcher("edit.jsp").forward(request, response);
        }
//processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold 

}
