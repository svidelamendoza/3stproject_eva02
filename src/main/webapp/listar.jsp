<%-- 
    Document   : index
    Created on : 25-04-2020, 1:18:45
    Author     : Sthephania
--%>
<%@page import="java.lang.String"%>
<%@page import="java.util.Iterator"%>
<%@page import="modelo.Cliente"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<!doctype html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>CIISA-Eva02</title>
    </head>
    <body>

        <!-- Default form ingreso de datos -->


        <p class="h4 mb-4"><h3><center>Mantenedor de Clientes Aethernus Corp</center></h3></p>

        <center><p>Listado de clientes</p></center>

        <!--aqui tengo que poner el diseño de la tabla -->
        <%
            List<Cliente> clientes = new ArrayList<>(); //Diamond modificado

            if (request.getAttribute("clientes") != null) {
                clientes = (List<Cliente>) request.getAttribute("clientes");

            }
            Iterator<Cliente> itClientes = clientes.iterator();
        %> 
        <form class="text-center border border-light p-5" action="controller_editar" method="POST">
            <table class="table">
                <thead>

                <th scope="col">#ID</th>
                <th scope="col">RUT</th>
                <th scope="col">DV</th>
                <th scope="col">Nombres</th>
                <th scope="col">Apellido Paterno</th>
                <th scope="col">Apellido Materno</th>
                <th scope="col">Seleccionar</th>


                </thead>
                <tbody>
                    <%while (itClientes.hasNext()) {
                            Cliente cli = itClientes.next();%>
                    <tr>
                        <td><%=cli.getCliID()%></td>
                        <td><%=cli.getCliRut()%></td>
                        <td><%=cli.getCliDV()%></td>
                        <td><%=cli.getCliNombres()%></td>
                        <td><%=cli.getCliApPaterno()%></td>
                        <td><%=cli.getCliApMaterno()%></td>
                        <td><input type="radio" name="seleccion" value="<%=cli.getCliID()%>"></td>
                    </tr>
                    <%}%>                
                </tbody>         
            </table>
                
            <center><p>INDICACIONES: Seleccione el registro y luego la opción a realizar:</p></center>
            <!-- botones submit -->
            <a class="btn btn-info btn-" href="registrar.jsp">>>Registrar</a>
            <td><button type ="submit" name ="accion" class="btn btn-success" value="editar" >>>Editar</button></td>
            <td><button type ="submit" name ="accion" class="btn btn-danger" value="eliminar">>>Eliminar</button></td>

            <p></p>
            <td><a class="btn btn-info btn-block" href="index.jsp" >>>Inicio</a></td>


             
            
        </form>


        <!-- Optional JavaScript -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    </body>
</html>
