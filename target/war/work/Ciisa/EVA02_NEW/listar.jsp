<%-- 
    Document   : index
    Created on : 25-04-2020, 1:18:45
    Author     : Sthephania
--%>
<%@page import="java.lang.String"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Iterator"%>
<%@page import="modelo.Cliente"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<!doctype html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>CIISA-Eva02</title>
    </head>
    <body>

        <!-- Default form ingreso de datos -->


        <p class="h4 mb-4">Mantenedor de Clientes Aethernus Corp</p>

        <p>Listado de clientes</p>

        <!--aqui tengo que poner el diseño de la tabla qla-->
        <%
            DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
            //SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
            List<Cliente> clientes = new ArrayList<>(); //Diamond modificado

            if (request.getAttribute("clientes") != null) {
                clientes = (List<Cliente>) request.getAttribute("clientes");

            }
            Iterator<Cliente> itClientes = clientes.iterator();
        %> 
        <form class="text-center border border-light p-5" action="controller_edit" method="POST">
            <table class="table">
                <thead>

                <th scope="col">#ID</th>
                <th scope="col">RUT</th>
                <th scope="col">DV</th>
                <th scope="col">Nombres</th>
                <th scope="col">Apellido Paterno</th>
                <th scope="col">Apellido Materno</th>


                </thead>
                <tbody>
                    <%while (itClientes.hasNext()) {
                            Cliente cli = itClientes.next();%>
                    <tr>
                        <td><%= cli.getCliID()%></td>
                        <td><%= cli.getCliRut()%></td>
                        <td><%= cli.getCliDV()%></td>
                        <td><%= cli.getCliNombres()%></td>
                        <td><%= cli.getCliApPaterno()%></td>
                        <td><%= cli.getCliApMaterno()%></td>
                        <td></td>
                        <!--<td><p data-placement="top" data-toggle="tooltip" title="Edit"><button class="btn btn-primary btn-xs" data-title="Edit" data-toggle="modal" data-target="#edit" ><span class="glyphicon glyphicon-pencil"></span></button></p></td>
                        <td><p data-placement="top" data-toggle="tooltip" title="Delete"><button class="btn btn-danger btn-xs" data-title="Delete" data-toggle="modal" data-target="#delete" ><span class="glyphicon glyphicon-trash"></span></button></p></td>-->
                        <td> <input type="radio" name="seleccion" value="<%= cli.getCliID()%>" style = "display: inline-block;vertical-align: top;" required="true"> </td>

                    </tr>
                    <%}%>                
                </tbody>         

            </table>

            <!-- botones submit -->
            <button type="submit" name="accion" value="editar" class="btn btn-lg btn-info " ><span class="glyphicon glyphicon-pencil"></span> Editar</button>

            <button type="submit" name="accion" value="eliminar" class="btn btn-lg btn-danger"><span class="glyphicon glyphicon-trash"></span> Eliminar </button>


        </form>
        <a class="btn btn-info btn-block" href=".index.jsp">Menú</a>

        <!-- Optional JavaScript -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    </body>
</html>
