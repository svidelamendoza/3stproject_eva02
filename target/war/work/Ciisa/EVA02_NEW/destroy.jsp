<%-- 
    Document   : index
    Created on : 25-04-2020, 1:18:45
    Author     : Sthephania
--%>
<%@page import="java.lang.String"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>CIISA-Eva02</title>
  </head>
  <body>
                   
        <!-- Default form ingreso de datos -->
    <form class="text-center border border-light p-5" action="controller_destroy" method="POST">

    <p class="h4 mb-4">Mantenedor de Clientes Aethernus Corp</p>

    <p>Borre los datos del registro:</p>

    <!-- ID -->
  	ID:
    <input type="number" name="cli_id" value="" id="defaultSubscriptionFormPassword" class="form-control mb-4" placeholder="001">
   
    <!-- RUT -->
  	RUT:
    <input type="number" name="cli_rut" value="" id="defaultSubscriptionFormPassword" class="form-control mb-4" placeholder="12345678">
    
    <!-- DV -->
  	DV:
    <input type="number" name="cli_dv" value="" id="defaultSubscriptionFormPassword" class="form-control mb-4" placeholder="K">
    
    <!--Nombres-->
  	nombres:
    <input type="text" name="cli_nombres" value="" id="defaultSubscriptionFormPassword" class="form-control mb-4" placeholder="Juan Andrés">

    <!--Apellido Paterno-->
  	Apellido Paterno:
    <input type="text" name="cli_ap_paterno" value="" id="defaultSubscriptionFormPassword" class="form-control mb-4" placeholder="Pérez">

    <!--Apellido Materno-->
  	Apellido Materno:
    <input type="text" name="cli_ap_materno" value="" id="defaultSubscriptionFormPassword" class="form-control mb-4" placeholder="Reyes">


    <!-- botones submit -->
    <button <input class="btn btn-info btn-block" type="submit">Borrar</button>
     <p></p>
    <a class="btn btn-info btn-block" href="./Vistas/listar.jsp" type="submit">Volver</a>
              
        </form>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
  </body>
</html>