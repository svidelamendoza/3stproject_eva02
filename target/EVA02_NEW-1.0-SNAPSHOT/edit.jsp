<%-- 
    Document   : index
    Created on : 25-04-2020, 1:18:45
    Author     : Sthephania
--%>
<%@page import="modelo.Cliente"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>CIISA-Eva02</title>
  </head>
  
    <%
        Cliente cli = (Cliente) request.getAttribute("cliente");
    %>
  <body>
                   
        <!-- Default form ingreso de datos -->
        
    <form class="text-center border border-light p-5" action="controller_Graba" method="POST">

    <p class="h4 mb-4">Mantenedor de Clientes Aethernus Corp</p>

    <p>Edite los datos del registro:</p>

     <!-- ID -->
  	Modificar ID:
    <input type="text" class="form-control" value="" id="" name="cli_id" placeholder="<%=cli.getCliID()%>"disabled> 
    <input type="text" name="cli_id" value="<%=cli.getCliID()%>" id="" class="form-control mb-4" placeholder="<%=cli.getCliID()%>">
   
    <!-- RUT -->
  	Modificar RUT:
    <input type="text" class="form-control" value="" id="id" name="cli_rut" placeholder="<%= cli.getCliRut()%>" disabled> 
    <input type="tex" name="cli_rut" value="<%= cli.getCliRut()%>" id="defaultSubscriptionFormPassword" class="form-control mb-4" placeholder="">
    
    <!-- DV -->
  	Modificar DV:
    <input type="text" class="form-control" value="" id="id" name="cli_dv" placeholder="<%= cli.getCliDV()%>" disabled> 
    <input type="text" name="cli_dv" value="<%= cli.getCliDV()%>" id="defaultSubscriptionFormPassword" class="form-control mb-4" placeholder="">
    
    <!--Nombres-->
  	Modificar nombres:
    <input type="text" class="form-control" value="" id="id" name="cli_nombres" placeholder="<%= cli.getCliNombres()%>" disabled> 
    <input type="text" name="cli_nombres" value="<%= cli.getCliNombres()%>" id="defaultSubscriptionFormPassword" class="form-control mb-4" placeholder="">

    <!--Apellido Paterno-->
  	Modificar Apellido Paterno:
    <input type="text" class="form-control" value="" id="id" name="cli_ap_paterno" placeholder="<%= cli.getCliApPaterno()%>" disabled> 
    <input type="text" name="cli_ap_paterno" value="<%= cli.getCliApPaterno()%>" id="defaultSubscriptionFormPassword" class="form-control mb-4" placeholder="">

    <!--Apellido Materno-->
  	Modificar Apellido Materno:
    <input type="text" class="form-control" ivalue="" d="id" name="cli_ap_materno" placeholder="<%= cli.getCliApMaterno()%>" disabled> 
    <input type="text" name="cli_ap_materno" value="<%= cli.getCliApMaterno()%>" id="defaultSubscriptionFormPassword" class="form-control mb-4" placeholder="">

    <!-- botones submit -->
        <button <input name ="accion" class="btn btn-success" type="submit" value = "grabarEditar">>>Guardar Cambios</button>
        <p></p>
    
            <a class="btn btn-info btn-block" href="./controller_list">>>Listado</a>
     <p></p>

              
        </form>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
  </body>
</html>